# check letter and number
def isPalindrome(s: str):
    l = []
    s = s.lower()
    for ch in s:
        if ch.isalpha() or ch.isdigit():
            l.append(ch)
    return l == l[::-1]


print(isPalindrome("A man, a plan, a canal: Panama"))
