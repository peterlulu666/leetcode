import itertools


# def findSubstring(s: str, words: list):
#     word_permutation = list(itertools.permutations(words))
#     word_permutation = ["".join(tup) for tup in word_permutation]
#     res = []
#     for i in range(0, len(s)):
#         if s[i:i + len(word_permutation[0])] in word_permutation:
#             res.append(i)
#     return res

# Create slicing window using substring and the length is the number of character in all word
# Convert substring to list of word and the length is the number of character in one word
# Check word.sort list and sub_word.sort list
def findSubstring(s: str, words: list):
    all_word_len = len("".join(words))
    one_word_len = len(words[0])
    res = []
    words.sort()
    for i in range(0, len(s) - all_word_len + 1):
        sub_s = s[i:i + all_word_len]
        sub_word = [sub_s[j:j + one_word_len] for j in range(0, all_word_len, one_word_len)]
        sub_word.sort()
        if words == sub_word:
            res.append(i)
    return res


print(findSubstring("barfoothefoobarman", ["foo", "bar"]))
