def threeSumClosest(nums: list, target: int):
    nums.sort()
    diff = float("inf")
    closest = 0
    for i in range(0, len(nums)):
        left_ptr = i + 1
        right_ptr = len(nums) - 1
        while left_ptr < right_ptr:
            add_up = nums[i] + nums[left_ptr] + nums[right_ptr]
            if abs(add_up - target) < diff:
                diff = abs(add_up - target)
                closest = add_up
            if add_up == target:
                return target
            elif add_up < target:
                left_ptr = left_ptr + 1
            else:
                right_ptr = right_ptr - 1
    return closest


print(threeSumClosest([-1, 2, 1, -4], 1))
