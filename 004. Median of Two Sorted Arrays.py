# merge list
# sort list
# check if it is even or odd length

def findMedianSortedArrays(nums1: list, nums2: list):
    merge_list = nums1 + nums2
    merge_list.sort()
    if len(merge_list) % 2 != 0:
        mid_index = (len(merge_list) + 1) // 2
        mid_index = mid_index - 1
        return float(merge_list[mid_index])
    else:
        mid_left_index = len(merge_list) // 2
        mid_left_index = mid_left_index - 1
        mid_right_index = (len(merge_list) // 2) + 1
        mid_right_index = mid_right_index - 1
        return float((merge_list[mid_left_index] + merge_list[mid_right_index]) / 2)


print(findMedianSortedArrays([1, 2], [3, 4]))
