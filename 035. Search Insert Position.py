# Binary search
# If there is no target, return left pointer
def searchInsert(nums: list, target: int):
    left_pointer = 0
    right_pointer = len(nums) - 1
    while left_pointer <= right_pointer:
        mid_pointer = (left_pointer + right_pointer) // 2
        if target == nums[mid_pointer]:
            return mid_pointer
        if target < nums[mid_pointer]:
            right_pointer = mid_pointer - 1
        else:
            left_pointer = mid_pointer + 1
    return left_pointer


print(searchInsert([1, 3, 5, 6], 2))
