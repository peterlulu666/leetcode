class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


# # ptr1 for l1, ptr2 for l2, dummy and cur for l3
def mergeTwoLists(list1: ListNode, list2: ListNode):
    l1_ptr = list1
    l2_ptr = list2
    dummy = ListNode()
    cur = dummy
    while l1_ptr is not None or \
            l2_ptr is not None:
        val1 = l1_ptr.val if l1_ptr else float("inf")
        val2 = l2_ptr.val if l2_ptr else float("inf")
        if val1 < val2:
            cur.next = l1_ptr
            cur = cur.next
            l1_ptr = l1_ptr.next
        else:
            cur.next = l2_ptr
            cur = cur.next
            l2_ptr = l2_ptr.next
    return dummy.next


# def mergeTwoLists(list1: ListNode, list2: ListNode):
#     number_list = []
#     for node in [list1, list2]:
#         while node is not None:
#             number_list.append(node.val)
#             node = node.next
#     number_list.sort()
#     dummy = ListNode()
#     cur = dummy
#     for number in number_list:
#         cur.next = ListNode(number)
#         cur = cur.next
#     cur.next = None
#     return dummy.next


l11 = ListNode(1)
l12 = ListNode(2)
l13 = ListNode(4)
l11.next = l12
l12.next = l13
l21 = ListNode(1)
l22 = ListNode(3)
l23 = ListNode(4)
l21.next = l22
l22.next = l23

cur = mergeTwoLists(l11, l21)
while cur is not None:
    print(cur.val)
    cur = cur.next
