# def removeDuplicates(nums: list):
#     num_set = set(nums)
#     nums[:len(num_set)] = sorted(list(num_set))
#     return len(num_set)


# left pointer and right pointer start at index 1
# compare right pointer with prev
# if same move right, if not same replace left and move left and right
def removeDuplicates(nums: list):
    left_pointer = 1
    right_pointer = 1
    while right_pointer < len(nums):
        if nums[right_pointer] == nums[right_pointer - 1]:
            right_pointer = right_pointer + 1
        else:
            nums[left_pointer] = nums[right_pointer]
            left_pointer = left_pointer + 1
            right_pointer = right_pointer + 1
    return left_pointer


print(removeDuplicates([1, 1, 2]))
