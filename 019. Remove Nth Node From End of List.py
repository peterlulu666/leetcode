# Definition for singly-linked list.
class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


# store to list
# # remove
# # link the broken area and let list[-1] pointing to None
# # special check the len 0, len 0 and n 1
# def removeNthFromEnd(head: ListNode, n: int):
#     cur = head
#     number_list = []
#     while cur is not None:
#         number_list.append(cur)
#         cur = cur.next
#
#     old_index = len(number_list) - n
#     del number_list[old_index]
#
#     if len(number_list) == 0:
#         return None
#     if len(number_list) == 1:
#         number_list[0].next = None
#         return number_list[0]
#     if n == 1:
#         number_list[-1].next = None
#     else:
#         number_list[old_index - 1].next = number_list[old_index]
#         number_list[-1].next = None
#     return number_list[0]


# the left ptr is dummy ptr
# the right ptr is to move n distance
# move right ptr to tail
# left ptr pointing to next.next
# return dummy.next
def removeNthFromEnd(head: ListNode, n: int):
    dummy = ListNode()
    left_ptr = dummy
    left_ptr.next = head
    right_ptr = head
    for i in range(0, n - 1):
        right_ptr = right_ptr.next
    while right_ptr.next is not None:
        left_ptr = left_ptr.next
        right_ptr = right_ptr.next
    left_ptr.next = left_ptr.next.next
    return dummy.next


l1 = ListNode(1)
l2 = ListNode(2)
l3 = ListNode(3)
l4 = ListNode(4)
l5 = ListNode(5)
l1.next = l2
l2.next = l3
l3.next = l4
l4.next = l5
cur = removeNthFromEnd(l1, 5)
while cur is not None:
    print(cur.val)
    cur = cur.next
