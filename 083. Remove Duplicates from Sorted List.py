class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


# linkedlist to list to set to list to linkedlist
# def deleteDuplicates(head: ListNode):
#     number_list = []
#     while head is not None:
#         number_list.append(head.val)
#         head = head.next
#     number_list = list(set(number_list))
#     number_list.sort()
#     dummy = ListNode()
#     cur = dummy
#     for number in number_list:
#         node = ListNode(number)
#         cur.next = node
#         cur = cur.next
#     return dummy.next


# compare putside loop with inside loop
# def deleteDuplicates(head: ListNode):
#     cur = head
#     while cur is not None:
#         inside_cur = cur.next
#         while True:
#             if inside_cur is None:
#                 cur.next = None
#                 break
#             if cur.val == inside_cur.val:
#                 inside_cur = inside_cur.next
#             else:
#                 cur.next = inside_cur
#                 break
#         cur = cur.next
#     return head


# left pointer nad right pointer
def deleteDuplicates(head: ListNode):
    if head is None:
        return None
    dummy = ListNode()
    cur = dummy
    left_ptr = head
    right_ptr = head.next
    cur.next = left_ptr
    cur = cur.next
    while True:
        if right_ptr is None:
            cur.next = None
            break
        if left_ptr.val == right_ptr.val:
            right_ptr = right_ptr.next
        else:
            cur.next = right_ptr
            cur = cur.next
            left_ptr = right_ptr
            right_ptr = right_ptr.next
    return dummy.next


l1 = ListNode(1)
l2 = ListNode(1)
l3 = ListNode(2)
l1.next = l2
l2.next = l3

cur = deleteDuplicates(l1)
while cur is not None:
    print(cur.val)
    cur = cur.next
