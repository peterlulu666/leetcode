# Binary search
# Compare target with mid pointer * mid pointer
def mySqrt(x: int):
    left_pointer = 0
    right_pointer = x
    while left_pointer <= right_pointer:
        mid_pointer = (left_pointer + right_pointer) // 2
        if x < mid_pointer * mid_pointer:
            right_pointer = mid_pointer - 1
        elif x > mid_pointer * mid_pointer:
            left_pointer = mid_pointer + 1
        else:
            return mid_pointer
    return right_pointer


print(mySqrt(8))
