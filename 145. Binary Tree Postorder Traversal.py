class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


class Solution:
    def postorderTraversal(self, root: TreeNode):
        if root is None:
            return None

        def postorder(node, node_list):
            if node is None:
                return
            postorder(node.left, node_list)
            postorder(node.right, node_list)
            node_list.append(node.val)

        res = []
        postorder(root, res)
        return res
