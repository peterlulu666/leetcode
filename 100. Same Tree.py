class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# 2 None root, true
# 1 None root, false
# 0 None root, check value not same, false
# recursively check child node
class solution:
    def isSameTree(self, p: TreeNode, q: TreeNode):
        if (p is None) and (q is None):
            return True
        if (p is None) and (q is not None):
            return False
        if (p is not None) and (q is None):
            return False
        if p.val != q.val:
            return False
        return self.isSameTree(p.left, q.left) and self.isSameTree(p.right, q.right)
