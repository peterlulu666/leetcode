class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# preorder first time visited
# inorder second time visited
# postorder third time visited
def inorder(root, val_list):
    if root is None:
        return
    inorder(root.left, val_list)
    val_list.append(root.val)
    inorder(root.right, val_list)


def inorderTraversal(root: TreeNode):
    val_list = []
    inorder(root, val_list)
    return val_list


node1 = TreeNode(1)
node2 = TreeNode(2)
node3 = TreeNode(3)

node1.left = None
node1.right = node2

node2.left = node3
node2.right = None

node3.left = None
node3.right = None

print(inorderTraversal(node1))
