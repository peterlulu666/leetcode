def myAtoi(s: str):
    s = s.strip()
    concat_s = ""
    for character in s:
        # check start with - + number
        if (concat_s == "") and (character in ["+", "-"] or character.isdigit()):
            concat_s = concat_s + character
        # all other character are digit
        elif character.isdigit():
            concat_s = concat_s + character
        # if there is character that is not digit, then stop
        else:
            break
    # check if it contain only +
    # check if it contain only -
    # check if it is empty string
    if concat_s in ["+", "-", ""]:
        return 0
    int_s = int(concat_s)
    # check range
    if int_s < -2 ** 31:
        return -2 ** 31
    elif int_s > 2 ** 31 - 1:
        return 2 ** 31 - 1
    else:
        return int_s


print(myAtoi("+1"))
