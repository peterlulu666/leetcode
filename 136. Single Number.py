# sort list
# check adjacent number
def singleNumber(nums: list):
    if len(nums) == 1:
        return nums[0]
    nums.sort()
    for left_ptr in range(0, len(nums), 2):
        if left_ptr == len(nums) - 1:
            return nums[left_ptr]
        right_ptr = left_ptr + 1
        if nums[left_ptr] != nums[right_ptr]:
            return nums[left_ptr]


# count the number of times of removing each number
# def singleNumber(nums: list):
#     while len(nums) > 0:
#         number = nums[0]
#         count = 0
#         try:
#             while True:
#                 nums.remove(number)
#                 count = count + 1
#         except:
#             pass
#         if count == 1:
#             return number
#     return


print(singleNumber([4, 1, 2, 1, 2]))
