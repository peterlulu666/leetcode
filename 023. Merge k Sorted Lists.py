class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def mergeKLists(lists: list):
    number_list = []
    for node in lists:
        while node is not None:
            number_list.append(node.val)
            node = node.next
    number_list.sort()
    dummy = ListNode()
    cur = dummy
    for number in number_list:
        cur.next = ListNode(number)
        cur = cur.next
    cur.next = None
    return dummy.next


l11 = ListNode(1)
l12 = ListNode(4)
l13 = ListNode(5)
l11.next = l12
l12.next = l13
l21 = ListNode(1)
l22 = ListNode(3)
l23 = ListNode(4)
l21.next = l22
l22.next = l23
l31 = ListNode(2)
l32 = ListNode(6)
l31.next = l32

cur = mergeKLists([l11, l21, l31])
while cur is not None:
    print(cur.val)
    cur = cur.next
