# def strStr(haystack: str, needle: str):
#     if len(needle) == 0:
#         return 0
#     return haystack.find(needle)

def strStr(haystack: str, needle: str):
    for i in range(0, len(haystack)):
        if haystack[i:i + len(needle)] == needle:
            return i
    return -1


print(strStr("hello", "ll"))
