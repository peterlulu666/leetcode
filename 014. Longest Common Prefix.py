# sort list use length
# compare index 1 word with other word
# check each character of index 1 word
def longestCommonPrefix(strs: list):
    sort_list = sorted(strs, key=len)
    for i_th_character in range(0, len(sort_list[0])):
        for word in sort_list:
            if sort_list[0][i_th_character] != word[i_th_character]:
                return sort_list[0][0:i_th_character]
    return sort_list[0]


print(longestCommonPrefix(["flower", "flow", "flight"]))
