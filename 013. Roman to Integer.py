# put normal together
# put special together
# add up all normal
# subtract special if exist
def romanToInt(s: str):
    add_up = {"I": 1,
              "V": 5,
              "X": 10,
              "L": 50,
              "C": 100,
              "D": 500,
              "M": 1000}
    subtract = {"IV": 2,
                "IX": 2,
                "XL": 20,
                "XC": 20,
                "CD": 200,
                "CM": 200}

    num = 0
    for roman in s:
        num = num + add_up[roman]
    for key in subtract:
        if key in s:
            num = num - subtract[key]
    return num


print(romanToInt("III"))
