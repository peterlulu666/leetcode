# def maxArea(height: list):
#     area = 0
#     for left_index in range(0, len(height)):
#         for right_index in range(left_index, len(height)):
#             left_h = height[left_index]
#             right_h = height[right_index]
#             h = min(left_h, right_h)
#             w = right_index - left_index
#             area = max(area, h * w)
#     return area


# initial left index, right index and max area
# as long as pointer does not arrive center
# compare current area and max area
# choose the small height and move the pointer
def maxArea(height: list):
    left_index = 0
    right_index = len(height) - 1
    max_area = 0
    while left_index < right_index:
        current_area = min(height[left_index], height[right_index]) * (right_index - left_index)
        max_area = max(max_area, current_area)
        if height[left_index] < height[right_index]:
            left_index = left_index + 1
        else:
            right_index = right_index - 1
    return max_area


print(maxArea([1, 8, 6, 2, 5, 4, 8, 3, 7]))
