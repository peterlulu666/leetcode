class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# check root
# postorder dfs
# check left right root exist
# 1 + min(left height, right height)
# class Solution:
#     def minDepth(self, root: TreeNode):
#         if root is None:
#             return 0
#         left_root = root.left
#         right_root = root.right
#         left_height = self.minDepth(left_root)
#         right_height = self.minDepth(right_root)
#         if left_root is None and \
#                 right_root is None:
#             return 1
#         if left_root is not None and \
#                 right_root is None:
#             return 1 + left_height
#         if left_root is None and \
#                 right_root is not None:
#             return 1 + right_height
#         return 1 + min(left_height, right_height)

# bfs
from collections import deque


class Solution:
    def minDepth(self, root: TreeNode):
        if root is None:
            return 0

        q = deque()
        q.append((root, 1))
        while len(q) > 0:
            node, depth = q.popleft()
            if node.left is None and \
                    node.right is None:
                return depth
            if node.left is not None:
                q.append((node.left, depth + 1))
            if node.right is not None:
                q.append((node.right, depth + 1))
