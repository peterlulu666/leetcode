# if it is the left parentheses, add current string to list
# if it is the right parentheses, do not add current string, pop list,
# and check current string with corresponding parentheses of popped
# note that at the point you pop, you check if the list is empty
# note that after traverse string, check if the list is empty
def isValid(s: str):
    if len(s) % 2 != 0:
        return False
    allowed_character = "()[]{}"
    if not all(character in allowed_character for character in s):
        return False
    parentheses_dict = {"(": ")",
                        "[": "]",
                        "{": "}"}
    parentheses_list = []
    for parentheses in s:
        if parentheses in parentheses_dict:
            parentheses_list.append(parentheses)
        else:
            try:
                parentheses_poped = parentheses_list.pop()
                if parentheses_dict[parentheses_poped] != parentheses:
                    return False
            except:
                return False
    if len(parentheses_list) != 0:
        return False
    return True


print(isValid("(([]){})"))
