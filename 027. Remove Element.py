# def removeElement(nums: list, val: int):
#     copy_nums = nums[:]
#     copy_nums = [num for num in copy_nums if num != val]
#     # try:
#     #     while True:
#     #         copy_nums.remove(val)
#     # except:
#     #     pass
#     nums[:len(copy_nums)] = copy_nums[:]
#     return len(copy_nums)


# left pointer and right pointer start at index 0
# compare right with valToBeRemoved
# if same move right, if not same replace left move left and right
def removeElement(nums: list, val: int):
    left_pointer = 0
    right_pointer = 0
    while right_pointer < len(nums):
        if nums[right_pointer] == val:
            right_pointer = right_pointer + 1
        else:
            nums[left_pointer] = nums[right_pointer]
            left_pointer = left_pointer + 1
            right_pointer = right_pointer + 1
    return left_pointer


print(removeElement([0, 1, 2, 2, 3, 0, 4, 2], 2))
