# check Palindrome s == s[::-1]
def longestPalindrome(s: str):
    if len(s) == 1:
        return s
    palindrome_list = []
    for i in range(0, len(s)):
        for j in range(i, len(s)):
            substr = s[i:j + 1]
            if substr == substr[::-1]:
                palindrome_list.append((len(substr), substr))
    palindrome_list.sort(reverse=True)
    return palindrome_list[0][1]


print(longestPalindrome("abbbb"))
