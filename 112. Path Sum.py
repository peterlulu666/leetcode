class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# check root False
# preorder dfs
# base case node is None False
# add up val and check if it is leaf and then check if it is target sum
class Solution:
    def hasPathSum(self, root: TreeNode, targetSum: int):
        if root is None:
            return False

        def preorder_dfs(node: TreeNode, cur_sum: int, targetSum: int):
            if node is None:
                return False
            cur_sum = cur_sum + node.val
            if node.left is None and node.right is None:
                if cur_sum == targetSum:
                    return True
                else:
                    return False
            found_left = preorder_dfs(node.left, cur_sum, targetSum)
            found_right = preorder_dfs(node.right, cur_sum, targetSum)
            return found_left or found_right

        return preorder_dfs(root, 0, targetSum)
