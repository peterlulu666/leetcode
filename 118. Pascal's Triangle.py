# first row is 1
# start building up the row from the second row
# create prev_row [0] + res[i - 1] + [0]
# add up each adjacent number in prev_row and append to cur_row
def generate(numRows: int):
    res = []
    res.append([1])
    for i in range(1, numRows):
        prev_row = [0] + res[i - 1] + [0]
        cur_row = []
        for i in range(0, len(prev_row) - 1):
            cur_row.append(prev_row[i] + prev_row[i + 1])
        res.append(cur_row)
    return res


print(generate(5))
