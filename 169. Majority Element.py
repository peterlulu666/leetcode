# count remove
# def majorityElement(nums: list):
#     appear = len(nums) // 2
#     while len(nums) > 0:
#         remove_number = nums[0]
#         count = 0
#         try:
#             while True:
#                 nums.remove(remove_number)
#                 count = count + 1
#                 if count > appear:
#                     return remove_number
#         except:
#             pass


# count frequency
def majorityElement(nums: list):
    count_dict = dict()
    appear = len(nums) // 2
    for num in nums:
        if num not in count_dict:
            count_dict[num] = 1
        else:
            count_dict[num] = count_dict[num] + 1
        if count_dict[num] > appear:
            return num


print(majorityElement([2, 2, 1, 1, 1, 2, 2]))
