def getRow(rowIndex: int):
    res = []
    res.append([1])
    for i in range(1, rowIndex + 1):
        prev_row = [0] + res[-1] + [0]
        cur_row = []
        for i in range(0, len(prev_row) - 1):
            cur_row.append(prev_row[i] + prev_row[i + 1])
        res.append(cur_row)
    return res[-1]

print(getRow(3))
