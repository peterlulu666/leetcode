import string


# reverse traverse string
# add up character sequence * 26^0, sequence * 26^1, sequence * 26^2, sequence * 26^3 and so on
def titleToNumber(columnTitle: str):
    all_character = string.ascii_uppercase
    character_sequence = dict()
    sequence = 1
    for ch in all_character:
        character_sequence[ch] = sequence
        sequence = sequence + 1
    add_up = 0
    power = 0
    for i in range(len(columnTitle) - 1, -1, -1):
        add_up = add_up + character_sequence[columnTitle[i]] * (26 ** power)
        power = power + 1
    return add_up


print(titleToNumber("BA"))
