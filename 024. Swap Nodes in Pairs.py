class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


# cur pointing to right pointer and move cur
# cur pointing to left pointer and move cur
def swapPairs(head: ListNode):
    if head is None:
        return None
    if head.next is None:
        return head
    dummy = ListNode()
    cur = dummy
    left_ptr = head
    right_ptr = head.next
    while True:
        new_left_ptr = None
        if left_ptr.next.next is not None:
            new_left_ptr = left_ptr.next.next
        new_right_ptr = None
        if right_ptr.next is not None:
            if right_ptr.next.next is not None:
                new_right_ptr = right_ptr.next.next
        cur.next = right_ptr
        cur = cur.next
        cur.next = left_ptr
        cur = cur.next
        # check there is 0 node in the after
        if new_left_ptr is None:
            cur.next = None
            break
        # check there is 1 node in the after
        if new_left_ptr is not None and \
                new_right_ptr is None:
            cur.next = new_left_ptr
            break
        left_ptr = new_left_ptr
        right_ptr = new_right_ptr
    return dummy.next


l11 = ListNode(1)
l12 = ListNode(2)
l13 = ListNode(3)
l14 = ListNode(4)
l11.next = l12
l12.next = l13
l13.next = l14

cur = swapPairs(l11)
while cur is not None:
    print(cur.val)
    cur = cur.next
