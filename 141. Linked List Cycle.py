class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


# check if node is visited
# visited_list
# visited_dict
# def hasCycle(head: ListNode):
#     if head is None:
#         return False
#     visited_list = []
#     while head is not None:
#         if head not in visited_list:
#             visited_list.append(head)
#         else:
#             return True
#         head = head.next
#     return False

# def hasCycle(head: ListNode):
#     if head is None:
#         return False
#     visited_dict = dict()
#     while head is not None:
#         if head not in visited_dict:
#             visited_dict[head] = None
#         else:
#             return True
#         head = head.next
#     return False

# slow move 1 distance
# fast move 2 distance
# slow and fast overlap
def hasCycle(head: ListNode):
    if head is None:
        return False
    slow = head
    fast = head
    try:
        while True:
            slow = slow.next
            fast = fast.next.next
            if slow == fast:
                return True
    except:
        pass
    return False
