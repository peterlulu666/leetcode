# initial n is 1 and n - 1 is 1
def climbStairs(n: int):
    if n == 0:
        return 0
    if n == 1:
        return 1
    dp = [0] * (n + 1)
    dp[-1] = 1
    dp[-2] = 1
    for i in range(n - 2, -1, -1):
        dp[i] = dp[i + 1] + dp[i + 2]
    return dp[0]


print(climbStairs(5))
