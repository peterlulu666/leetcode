# Given an array of integers nums and an integer target,
# return indices of the two numbers such that they add up
# to target.
#
# You may assume that each input would have exactly one solution,
# and you may not use the same element twice.
#
# You can return the answer in any order

# Example 1:
#
# Input: nums = [2,7,11,15], target = 9
# Output: [0,1]
# Explanation: Because nums[0] + nums[1] == 9, we return [0, 1].
#
# Example 2:
#
# Input: nums = [3,2,4], target = 6
# Output: [1,2]
#
# Example 3:
#
# Input: nums = [3,3], target = 6
# Output: [0,1]

# Solution
# compare each number with everything on its right

# def twoSum(nums, target):
#     for i in range(0, len(nums) - 1):
#         for j in range(i + 1, len(nums)):
#             if target == nums[i] + nums[j]:
#                 return [i, j]


# Solution
# Check if complement = (target - number) is in dict
# initial dict include number index pair
# The (target - number) can not be itself

# def twoSum(nums, target):
#     # key is number and value is index
#     # If there is duplicated key, the value is going to updated
#     num_dict = dict()
#     for i in range(0, len(nums)):
#         num_dict[nums[i]] = i
#     for i in range(0, len(nums)):
#         complement = target - nums[i]
#         if (complement in num_dict) and (num_dict[complement] != i):
#             return [i, num_dict[complement]]

# Solution
# Check if complement = (target - number) is in dict
# initial dict is empty

# def twoSum(nums, target):
#     num_dict = dict()
#     for i in range(0, len(nums)):
#         complement = target - nums[i]
#         if complement not in num_dict:
#             num_dict[nums[i]] = i
#         else:
#             return [i, num_dict[complement]]

# Solution
# sort list of tuple
# if add up small, move left pointer
# if add up large, move right pointer

def twoSum(nums, target):
    temp = []
    for i in range(0, len(nums)):
        temp.append((nums[i], i))
    nums = temp
    nums.sort()
    left_ptr = 0
    right_ptr = len(nums) - 1
    while left_ptr < right_ptr:
        if nums[left_ptr][0] + nums[right_ptr][0] == target:
            return [nums[left_ptr][1], nums[right_ptr][1]]
        elif nums[left_ptr][0] + nums[right_ptr][0] < target:
            left_ptr = left_ptr + 1
        else:
            right_ptr = right_ptr - 1


# def twoSum(nums, target):
#     original_nums = nums[:]
#     left_ptr = 0
#     right_ptr = len(nums) - 1
#     nums.sort()
#     while left_ptr < right_ptr:
#         if nums[left_ptr] + nums[right_ptr] == target:
#             # https://stackoverflow.com/questions/22267241/how-to-find-the-index-of-the-nth-time-an-item-appears-in-a-list
#             # if two number are same, return the first occurrence and the second occurrence
#             if nums[left_ptr] == nums[right_ptr]:
#                 return [original_nums.index(nums[left_ptr]), [i for i, n in enumerate(original_nums) if n == nums[right_ptr]][1]]
#             return [original_nums.index(nums[left_ptr]), original_nums.index(nums[right_ptr])]
#         elif nums[left_ptr] + nums[right_ptr] < target:
#             left_ptr = left_ptr + 1
#         else:
#             right_ptr = right_ptr - 1


print(twoSum([3, 2, 4], 6))
