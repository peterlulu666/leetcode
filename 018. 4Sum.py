# i, j, left_ptr, right_ptr
# def fourSum(nums: list, target: int):
#     nums.sort()
#     sum_set = set()
#     for i in range(0, len(nums) - 3):
#         for j in range(i + 1, len(nums) - 2):
#             left_ptr = j + 1
#             right_ptr = len(nums) - 1
#             while left_ptr < right_ptr:
#                 add_up = nums[i] + nums[j] + nums[left_ptr] + nums[right_ptr]
#                 if add_up < target:
#                     left_ptr = left_ptr + 1
#                 elif add_up > target:
#                     right_ptr = right_ptr - 1
#                 else:
#                     sum_set.add((nums[i], nums[j], nums[left_ptr], nums[right_ptr]))
#                     left_ptr = left_ptr + 1
#                     while left_ptr < right_ptr and nums[left_ptr] == nums[left_ptr - 1]:
#                         left_ptr = left_ptr + 1
#
#     sum_list = [list(tup) for tup in sum_set]
#     return sum_list


def fourSum(nums: list, target: int):
    def kSum(nums_list: list, target_number: int, k: int, result: list, results: list):
        if len(nums_list) < k or k < 2 or target_number < nums_list[0] * k or target_number > nums_list[
            len(nums_list) - 1] * k:
            return []
        if k == 2:
            left_ptr = 0
            right_ptr = len(nums_list) - 1
            while left_ptr < right_ptr:
                add_up = nums_list[left_ptr] + nums_list[right_ptr]
                if add_up == target_number:
                    results.append(result + [nums_list[left_ptr], nums_list[right_ptr]])
                    left_ptr = left_ptr + 1
                    while left_ptr < right_ptr and nums_list[left_ptr] == nums_list[left_ptr - 1]:
                        left_ptr = left_ptr + 1
                elif add_up < target_number:
                    left_ptr = left_ptr + 1
                else:
                    right_ptr = right_ptr - 1
        else:
            for i in range(0, len(nums_list) - k + 1):
                kSum(nums_list[i + 1:len(nums_list)], target_number - nums_list[i], k - 1, result + [nums_list[i]],
                     results)

    nums.sort()
    results = []
    kSum(nums, target, 4, [], results)
    results = [tuple(l) for l in results]
    results = set(results)
    results = list(results)
    results = [list(tup) for tup in results]
    return results


print(fourSum([2, 2, 2, 2, 2], 8))
