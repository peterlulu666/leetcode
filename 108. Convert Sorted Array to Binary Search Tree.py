class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# mid is root
# root.left is left mid
# root.right is right mid
class Solution:
    def sortedArrayToBST(self, nums: list):
        return self.create_node(0, len(nums) - 1, nums)

    def create_node(self, left_ptr, right_ptr, number_list):
        if left_ptr > right_ptr:
            return None
        mid_ptr = (left_ptr + right_ptr) // 2
        root_node = TreeNode(number_list[mid_ptr])
        root_node.left = self.create_node(left_ptr, mid_ptr - 1, number_list)
        root_node.right = self.create_node(mid_ptr + 1, right_ptr, number_list)
        return root_node
