# 26 positional numeral
# def convertToTitle(columnNumber: int):
#     res = ""
#     while columnNumber > 0:
#         columnNumber = columnNumber - 1
#         res = res + chr(ord("A") + columnNumber % 26)
#         columnNumber = columnNumber // 26
#     return res[::-1]

import string


# 0 z, a 1, b 2 and so on
def convertToTitle(columnNumber: int):
    all_character = string.ascii_uppercase
    sequence_character = dict()
    sequence = 1
    for character in all_character[:-1]:
        sequence_character[sequence] = character
        sequence = sequence + 1
    sequence_character[0] = "Z"
    res = ""
    while columnNumber > 0:
        res = (sequence_character[columnNumber % 26]) + res
        columnNumber = columnNumber - 1
        columnNumber = columnNumber // (26)
    return res


print(convertToTitle(52))
