class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


def addTwoNumbers(l1: ListNode, l2: ListNode):
    dummy = ListNode()
    cur = dummy
    carry = 0

    while l1 or l2 or carry:
        # 7 + 8 = 15
        # val1 + val2 = new_value
        # divide the new_value 15 into carry and new_digit
        # cur pointer point to null
        # next pointer point to new_digit

        # get val1 and val2
        val1 = l1.val if l1 else 0
        val2 = l2.val if l2 else 0

        # get new_value and let the next pointer pint to the new_digit
        new_value = carry + val1 + val2
        carry = new_value // 10
        new_digit = new_value % 10
        cur.next = ListNode(new_digit)

        # update pointer
        l1 = l1.next if l1 else None
        l2 = l2.next if l2 else None
        cur = cur.next

    return dummy.next


# We calculate 123 + 456
num1 = ListNode(1)
num2 = ListNode(2)
num3 = ListNode(3)
num3.next = num2
num2.next = num1

num4 = ListNode(4)
num5 = ListNode(5)
num6 = ListNode(6)
num6.next = num5
num5.next = num4

# 3 -> 2 -> 1
# 6 -> 5 -> 4
print(addTwoNumbers(num3, num6))

# num_pointer = addTwoNumbers(num3, num6)
# while num_pointer is not None:
#     print(num_pointer.val, end=" -> ")
#     num_pointer = num_pointer.next

num_pointer = addTwoNumbers(num3, num6)
while True:
    print(num_pointer.val, end="")
    num_pointer = num_pointer.next
    if num_pointer is None:
        break
    print(" -> ", end="")
