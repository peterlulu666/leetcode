# The number cannot be -
# convert to string and check palindrome
def isPalindrome(x: int):
    if x < 0:
        return False
    else:
        str_x = str(x)
        return str_x == str_x[::-1]


print(isPalindrome(121))
