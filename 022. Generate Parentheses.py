def generateParenthesis(n: int):
    def backtrack(openN, closeN, stack, result):
        if openN == closeN == n:
            result.append("".join(stack))
            return
        if openN < n:
            backtrack(openN + 1, closeN, stack + ["("], result)
        if closeN < openN:
            backtrack(openN, closeN + 1, stack + [")"], result)

    result = []
    backtrack(0, 0, [], result)
    return result


print(generateParenthesis(3))
