# remove -
# convert to str
# remove 0
# reverse
# check - and convert to int
# check range
def reverse(x: int):
    abs_x = abs(x)
    str_x = str(abs_x)
    str_x.replace("0", "")
    str_x = str_x[::-1]
    int_x = int(str_x)
    if x < 0:
        int_x = int_x * -1
    else:
        int_x = int_x
    if (int_x < -2 ** 31) or (int_x > 2 ** 31 - 1):
        return 0
    else:
        return int_x


print(reverse(1534236469))
