# initial left ptr and right ptr
# check if left num + right num == 0 - current num
# add three num tuple to set
# set of tuple to list of tuple
# list of tuple to list of list
def threeSum(nums: list):
    nums.sort()
    add_up_set = set()
    for i in range(0, len(nums)):
        left_ptr = i + 1
        right_ptr = len(nums) - 1
        target = 0 - nums[i]
        while left_ptr < right_ptr:
            if nums[left_ptr] + nums[right_ptr] == target:
                add_up_set.add((nums[i], nums[left_ptr], nums[right_ptr]))
                left_ptr = left_ptr + 1
                while left_ptr < right_ptr and nums[left_ptr] == nums[left_ptr - 1]:
                    left_ptr = left_ptr + 1
            elif nums[left_ptr] + nums[right_ptr] < target:
                left_ptr = left_ptr + 1
            else:
                right_ptr = right_ptr - 1
    add_up_list = list(add_up_set)
    add_up_list = [list(ele) for ele in add_up_list]
    return add_up_list


print(threeSum([-1, 0, 1, 2, -1, -4]))
