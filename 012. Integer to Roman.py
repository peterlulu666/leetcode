# put conversion into dictionary
# use // to count roman letter
# use % to calculate the remaining
def intToRoman(num: int):
    convert_dict = {1000: "M",
                    900: "CM",
                    500: "D",
                    400: "CD",
                    100: "C",
                    90: "XC",
                    50: "L",
                    40: "XL",
                    10: "X",
                    9: "IX",
                    5: "V",
                    4: "IV",
                    1: "I"}

    roman = ""
    for key in convert_dict:
        roman = roman + (num // key) * convert_dict[key]
        num = num % key
    return roman


print(intToRoman(3))
