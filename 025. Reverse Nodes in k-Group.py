class ListNode:
    def __init__(self, val=0, next=None):
        self.val = val
        self.next = next


# append node address to list
# traverse list and i is from 0 to len - remain - k + 1
# reverse each k_group and append to res
# append remain to res
# create new ListNode
def reverseKGroup(head: ListNode, k: int):
    node_list = []
    res = []
    while head is not None:
        node_list.append(head)
        head = head.next
    remain = len(node_list) % k
    for i in range(0, len(node_list) - remain - k + 1, k):
        k_group = node_list[i: i + k]
        k_group.reverse()
        res = res + k_group
    res = res + node_list[len(node_list) - remain: len(node_list)]
    dummy = ListNode()
    cur = dummy
    for node in res:
        cur.next = node
        cur = cur.next
    cur.next = None
    return dummy.next


l11 = ListNode(1)
l12 = ListNode(2)
l13 = ListNode(3)
l14 = ListNode(4)
l15 = ListNode(5)
l11.next = l12
l12.next = l13
l13.next = l14
l14.next = l15
cur = reverseKGroup(l11, 3)
while cur is not None:
    print(cur.val)
    cur = cur.next
