# def plusOne(digits: list):
#     digits = [str(number) for number in digits]
#     number_str = "".join(digits)
#     number_int = int(number_str)
#     number_int = number_int + 1
#     number_str = str(number_int)
#     return [int(digit_str) for digit_str in number_str]

def plusOne(digits: list):
    l1 = digits
    l1.reverse()
    l2 = [1]
    i = 0
    j = 0
    carry = 0
    add_up_list = []
    while i < len(l1) or j < len(l2) or carry:
        val1 = l1[i] if i < len(l1) else 0
        val2 = l2[j] if j < len(l2) else 0

        new_val = val1 + val2 + carry
        carry = new_val // 10
        new_digit = new_val % 10
        add_up_list.append(new_digit)

        i = i + 1 if i < len(l1) - 1 else len(l1)
        j = j + 1 if j < len(l2) - 1 else len(l2)
    add_up_list.reverse()
    return add_up_list


print(plusOne([9]))
