# def findMissingRanges(nums: list, lower: int, upper: int):
#     res = []
#     for num in nums:
#         if lower == num:
#             lower = lower + 1
#         elif lower < num:
#             if lower + 1 == num:
#                 res.append(str(lower))
#                 lower = num + 1
#             else:
#                 res.append(str(lower) + "->" + str(num - 1))
#                 lower = num + 1
#
#     if lower > upper:
#         return res
#     if lower == upper:
#         res.append(str(lower))
#         return res
#     if lower < upper:
#         res.append(str(lower) + "->" + str(upper))
#         return res


# def findMissingRanges(nums: list, lower: int, upper: int):
#     res = []
#     nums = nums + [upper + 1]
#     for num in nums:
#         if lower == num:
#             lower = num + 1
#         elif lower < num:
#             if lower + 1 == num:
#                 res.append(str(lower))
#                 lower = num + 1
#             else:
#                 res.append(str(lower) + "->" + str(num - 1))
#                 lower = num + 1
#     return res


# add upper + 1 to list
# compare lower with num
# if it is same move lower else append either one or more number
def findMissingRanges(nums: list, lower: int, upper: int):
    res = []
    nums = nums + [upper + 1]
    for num in nums:
        if lower < num:
            if lower + 1 == num:
                res.append(str(lower))
            else:
                res.append(str(lower) + "->" + str(num - 1))
        lower = num + 1
    return res


# lower 1, upper 100
# [0, 5, 10], impossible
# [1, 5, 10], continue
# [2, 5, 10], add str(lower)
# [3, 5, 10], add str(lower) -> list element - 1
print(findMissingRanges([0, 1, 3, 50, 75], 0, 99))
