class Solution:
    def isHappy(self, n: int) -> bool:
        number_set = set()
        while True:
          number_set.add(n)
          n = sum([int(element)**2 for element in str(n)])
          if n == 1:
            return True
          if n in number_set:
            return False