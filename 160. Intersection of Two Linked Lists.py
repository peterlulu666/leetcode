class ListNode:
    def __init__(self, x):
        self.val = x
        self.next = None


# check if nodeB can be found in dict_A
def getIntersectionNode(headA: ListNode, headB: ListNode):
    dict_A = dict()
    while headA is not None:
        dict_A[headA] = None
        headA = headA.next

    while headB is not None:
        if headB in dict_A:
            return headB
        headB = headB.next

    return None
