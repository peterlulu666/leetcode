import re


def isMatch(s: str, p: str):
    re_s = re.search(p, s)
    if re_s is None:
        return False
    else:
        if s == re_s.group():
            return True
        else:
            return False


print(isMatch("ab", ".*"))
