# add up two number
# // 2 and % 2
def addBinary(a: str, b: str):
    l1 = [int(character) for character in a]
    l1.reverse()
    l2 = [int(character) for character in b]
    l2.reverse()
    i = 0
    j = 0
    carry = 0
    add_up_list = []
    while i < len(l1) or j < len(l2) or carry:
        val1 = l1[i] if i < len(l1) else 0
        val2 = l2[j] if j < len(l2) else 0

        new_val = val1 + val2 + carry
        new_digit = new_val % 2
        carry = new_val // 2
        add_up_list.append(new_digit)

        i = i + 1
        j = j + 1
    add_up_list.reverse()
    return "".join([str(number) for number in add_up_list])


print(addBinary("11", "1"))
