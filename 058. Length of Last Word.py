def lengthOfLastWord(s: str):
    word_list = s.split()
    return len(word_list[-1])

print(lengthOfLastWord("Hello World"))
