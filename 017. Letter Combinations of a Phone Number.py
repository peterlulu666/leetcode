import itertools


def letterCombinations(digits: str):
    phone_keyboard = {2: "abc",
                      3: "def",
                      4: "ghi",
                      5: "jkl",
                      6: "mno",
                      7: "pqrs",
                      8: "tuv",
                      9: "wxyz"}
    if digits == "":
        return []
    if len(digits) == 1:
        return list(phone_keyboard[int(digits)])
    letter_list = []
    for number in digits:
        letter_list.append(phone_keyboard[int(number)])
    a = letter_list[0]
    for i in range(1, len(letter_list)):
        b = letter_list[i]
        # https://stackoverflow.com/questions/12935194/permutations-between-two-lists-of-unequal-length
        c = list(itertools.product(a, b))
        # "".join(tup) is as same as str(tup[0] + tup[1])
        a = [str(tup[0] + tup[1]) for tup in c]
    return a


print(letterCombinations("233"))
