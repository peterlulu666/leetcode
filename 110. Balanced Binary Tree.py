class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# postorder
# base case [0, True]
# other node [1 + max(left height, right height), (left balance, right balance) and
# (left height - right height <= 1)]
class Solution:
    def isBalanced(self, root: TreeNode):
        if root is None:
            return True

        def postorder_dfs(node: TreeNode):
            if node is None:
                return [0, True]
            left_height, left_balanced = postorder_dfs(node.left)
            right_height, right_balanced = postorder_dfs(node.right)
            return [1 + max(left_height, right_height),
                    (left_balanced and right_balanced) and abs(left_height - right_height) <= 1]

        return postorder_dfs(root)[1]
