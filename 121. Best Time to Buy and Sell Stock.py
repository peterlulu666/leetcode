# check right pointer - left pointer
# def maxProfit(prices: list):
#     profit = float("-inf")
#     for left_ptr in range(0, len(prices) - 1):
#         for right_ptr in range(left_ptr + 1, len(prices)):
#             profit = max(profit, prices[right_ptr] - prices[left_ptr])
#     return profit if profit > 0 else 0


# if left number is small move right
# if left number is big, move left pointer and right pointer together
def maxProfit(prices: list):
    left_ptr = 0
    right_ptr = 1
    profit = float("-inf")
    while right_ptr < len(prices):
        if prices[left_ptr] < prices[right_ptr]:
            profit = max(profit, prices[right_ptr] - prices[left_ptr])
            right_ptr = right_ptr + 1
        else:
            left_ptr = right_ptr
            right_ptr = right_ptr + 1
    return profit if profit > 0 else 0


print(maxProfit([7, 1, 5, 3, 6, 4]))
