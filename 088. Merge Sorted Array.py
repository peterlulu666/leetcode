# remove 0
# extend list
# list sort
# def merge(nums1: list, m: int, nums2: list, n: int):
#     nums1.reverse()
#     for i in range(0, n):
#         nums1.remove(0)
#     nums1.reverse()
#     nums1.extend(nums2)
#     nums1.sort()

# p1 pointing to m - 1
# p2 pointing to n - 1
# i pointing to len(nums1) - 1
def merge(nums1: list, m: int, nums2: list, n: int):
    if m == 0:
        nums1[:] = nums2[:]
        return
    if n == 0:
        nums1[:] = nums1[:]
        return
    p1 = m - 1
    p2 = n - 1
    i = len(nums1) - 1
    while p2 >= 0:
        number1 = nums1[p1] if p1 >= 0 else float("-inf")
        number2 = nums2[p2]
        if number1 > number2:
            nums1[i] = number1
            p1 = p1 - 1
            i = i - 1
        else:
            nums1[i] = number2
            p2 = p2 - 1
            i = i - 1
    return
