# [(count, [])]
# list of tuple
# tuple contain count and substring list
def lengthOfLongestSubstring(s: str):
    if len(s) == 0:
        return 0
    all_substring_list = []
    for i in range(0, len(s)):
        count_dict = {}
        count_dict[s[i]] = 1
        for j in range(i + 1, len(s)):
            if s[j] not in count_dict:
                count_dict[s[j]] = 1
            else:
                break
        all_substring_list.append((len(count_dict), list(count_dict)))

    all_substring_list.sort(reverse=True)
    return int(all_substring_list[0][0])


print(lengthOfLongestSubstring("abcabcbb"))
