class TreeNode:
    def __init__(self, val=0, left=None, right=None):
        self.val = val
        self.left = left
        self.right = right


# check root
# postorder dfs
# other node height is 1 + max(left_height, right_height)
# class Solution:
#     def maxDepth(self, root: TreeNode):
#         if root is None:
#             return 0
#
#         left_height = self.maxDepth(root.left)
#         right_height = self.maxDepth(root.right)
#         return 1 + max(left_height, right_height)


# bfs
# class Solution:
#     def maxDepth(self, root: TreeNode):
#         if root is None:
#             return 0
#
#         q = []
#         q.append((root, 1))
#         depth = 0
#         while len(q) > 0:
#             node, depth = q.pop(0)
#             if node.left is not None:
#                 q.append((node.left, depth + 1))
#             if node.right is not None:
#                 q.append((node.right, depth + 1))
#         return depth

from collections import deque


class Solution:
    def maxDepth(self, root: TreeNode):
        if root is None:
            return 0

        q = deque()
        q.append((root, 1))
        depth = 0
        while len(q) > 0:
            node, depth = q.popleft()
            if node.left is not None:
                q.append((node.left, depth + 1))
            if node.right is not None:
                q.append((node.right, depth + 1))
        return depth
